
- Containerisation  40%
  - create Dockerfile
  - test Dockerfile on other project



Pipeline 30%
  - create build ( binay and docker image )
  - push image into registry


Environment 20%

```console
 docker-compose -f ./deployments/docker-compose.yml up
Starting deployments_b_1 ... done
Starting deployments_a_1 ... done
Attaching to deployments_b_1, deployments_a_1
b_1  | service|2019/11/09 17:27:35 initialising service...
a_1  | service|2019/11/09 17:27:36 initialising service...
b_1  | service|2019/11/09 17:27:35 attempting to listen on '0.0.0.0:8000'...
b_1  |  server|2019/11/09 17:27:36 < localhost:8000 <- 127.0.0.1:45388 | HTTP/1.1 GET / 
b_1  | service|2019/11/09 17:27:36 > http://localhost:8000/ -> '200 OK'
a_1  | service|2019/11/09 17:27:36 attempting to listen on '0.0.0.0:8000'...
b_1  |  server|2019/11/09 17:27:37 < localhost:8000 <- 127.0.0.1:45390 | HTTP/1.1 GET / 
b_1  | service|2019/11/09 17:27:37 > http://localhost:8000/ -> '200 OK'
a_1  |  server|2019/11/09 17:27:37 < localhost:8000 <- 127.0.0.1:45392 | HTTP/1.1 GET / 
a_1  | service|2019/11/09 17:27:37 > http://localhost:8000/ -> '200 OK'
b_1  |  server|2019/11/09 17:27:38 < localhost:8000 <- 127.0.0.1:45394 | HTTP/1.1 GET / 
b_1  | service|2019/11/09 17:27:38 > http://localhost:8000/ -> '200 OK'
a_1  |  server|2019/11/09 17:27:38 < localhost:8000 <- 127.0.0.1:45396 | HTTP/1.1 GET / 
a_1  | service|2019/11/09 17:27:38 > http://localhost:8000/ -> '200 OK'
b_1  |  server|2019/11/09 17:27:39 < localhost:8000 <- 127.0.0.1:45398 | HTTP/1.1 GET / 
b_1  | service|2019/11/09 17:27:39 > http://localhost:8000/ -> '200 OK'
a_1  |  server|2019/11/09 17:27:39 < localhost:8000 <- 127.0.0.1:45400 | HTTP/1.1 GET / 
a_1  | service|2019/11/09 17:27:39 > http://localhost:8000/ -> '200 OK'
b_1  |  server|2019/11/09 17:27:40 < localhost:8000 <- 127.0.0.1:45402 | HTTP/1.1 GET / 
b_1  | service|2019/11/09 17:27:40 > http://localhost:8000/ -> '200 OK'
a_1  |  server|2019/11/09 17:27:40 < localhost:8000 <- 127.0.0.1:45404 | HTTP/1.1 GET / 
a_1  | service|2019/11/09 17:27:40 > http://localhost:8000/ -> '200 OK'
^CGracefully stopping... (press Ctrl+C again to force)
Stopping deployments_b_1 ... done
Stopping deployments_a_1 ... done

```

Documentation 10%


Versioning (Bonus) 20%
