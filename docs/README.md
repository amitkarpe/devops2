

## Getting Started
This guide will provide basic steps to start using [Gitlab CI](https://docs.gitlab.com/ee/ci/pipelines.html) ( Continuous Integration ).

### Prerequisites
Access to [repository](https://gitlab.com/amitkarpe/devops2),
Linux system with docker, docker-compose, git, etc. If planning to do full development then having golang installed on the system will be good option.

### Basic Installation
Get cloned on the local system:

```bash
git clone https://gitlab.com/amitkarpe/devops2
cd deveops2
ls -lh
```

Review Makefile to know which commands are availables.

e.g.

```bash
make dep
or
make build
or
make docker image

```

## List of target commands

### Predefine targets

Here is complete list of targets, which will explain the tasks it is expected to perform.

Target Name | Task's detail explantion
------------|--------------------------
dep | Installs golang dependencies "vendor" folder by checking go.mod and environment variable i.e. make vendored copy of dependencies
build | Builds the **pinger** binary program into *bin/* directory.
run | Run the **pinger** program on the local system
test | Automates testing the packages named by the  import paths i.e. devops/cmd/pinger
docker_image | Build the image **"devops/pinger"** using Dockerfile code
docker_testrun | Run the docker container, mapping the host port 8000 with container port
docker_tar | Build the image and save that image into tar file for future use
docker_untar | Load the **image.tar** into docker on the system
testenv | Test the docker compose, docker orchestration capabiltiy by running two images

### Used for review


To review the DevOps Pipeline, following targets need to use.

Target Name | Task's detail explantion
------------|--------------------------
verify_readme | This command will list the content of "./docs/README.md", which include this documentation 
verify_pipeline | This command will list the content of ./.gitlab-ci.yml, Which include code to manage Gitlab CI Pipeline

### Planned as extra

Planning to add two more options. **Pendding tasks**
- [ ] verify_code
- [ ] verify_k8s
- [ ] verify_openshift
- [ ] verify_gke


## Code explaination

### What is the program output?


```bash
root@50d092296e4c:/go/devops2# make dep
GO111MODULE=on go mod vendor
root@50d092296e4c:/go/devops2# make build
GO111MODULE=on go build -mod vendor -o ./bin/pinger ./cmd/pinger
root@50d092296e4c:/go/devops2# make run
GO111MODULE=on go run ./cmd/pinger
service|2019/11/10 08:33:27 initialising service...
service|2019/11/10 08:33:27 attempting to listen on '0.0.0.0:8000'...
 server|2019/11/10 08:33:28 < localhost:8000 <- 127.0.0.1:51498 | HTTP/1.1 GET / 
service|2019/11/10 08:33:28 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:33:29 < localhost:8000 <- 127.0.0.1:51500 | HTTP/1.1 GET / 
service|2019/11/10 08:33:29 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:33:30 < localhost:8000 <- 127.0.0.1:51502 | HTTP/1.1 GET / 
service|2019/11/10 08:33:30 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:33:31 < localhost:8000 <- 127.0.0.1:51504 | HTTP/1.1 GET / 
service|2019/11/10 08:33:31 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:33:32 < localhost:8000 <- 127.0.0.1:51506 | HTTP/1.1 GET / 
service|2019/11/10 08:33:32 > http://localhost:8000/ -> '200 OK'
^Cservice|2019/11/10 08:33:33 received termination signal 'interrupt', shutting down server now
service|2019/11/10 08:33:33 exiting now...
make: *** [Makefile:8: run] Interrupt

```

### What else this program is doing?


```bash
$ docker images devops/pinger 
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
devops/pinger       latest              329fc988ce71        15 hours ago        12.4MB

$ docker run -it -p 8000:8000 devops/pinger:latest

service|2019/11/10 08:36:26 initialising service...
service|2019/11/10 08:36:26 attempting to listen on '0.0.0.0:8000'...
 server|2019/11/10 08:36:27 < localhost:8000 <- 127.0.0.1:51542 | HTTP/1.1 GET / 
service|2019/11/10 08:36:27 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:36:28 < localhost:8000 <- 172.17.0.1:54894 | HTTP/1.1 GET /readyz 
 server|2019/11/10 08:36:28 < localhost:8000 <- 127.0.0.1:51548 | HTTP/1.1 GET / 
service|2019/11/10 08:36:28 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:36:29 < localhost:8000 <- 127.0.0.1:51554 | HTTP/1.1 GET / 
service|2019/11/10 08:36:35 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:36:36 < localhost:8000 <- 127.0.0.1:51570 | HTTP/1.1 GET / 
service|2019/11/10 08:36:36 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:36:37 < localhost:8000 <- 172.17.0.1:54924 | HTTP/1.1 GET /livez 
 server|2019/11/10 08:36:37 < localhost:8000 <- 127.0.0.1:51578 | HTTP/1.1 GET / 
service|2019/11/10 08:36:37 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:36:38 < localhost:8000 <- 127.0.0.1:51580 | HTTP/1.1 GET / 
service|2019/11/10 08:36:38 > http://localhost:8000/ -> '200 OK'
 server|2019/11/10 08:36:39 < localhost:8000 <- 127.0.0.1:51582 | HTTP/1.1 GET / 
service|2019/11/10 08:36:39 > http://localhost:8000/ -> '200 OK'
^Cservice|2019/11/10 08:36:40 received termination signal 'interrupt', shutting down server now
service|2019/11/10 08:36:40 exiting now...

$ curl http://localhost:8000/readyz    
curl: (7) Failed to connect to localhost port 8000: Connection refused
$ curl http://localhost:8000/readyz
not ready%                       
$ curl http://localhost:8000/livez 
hello world%                               

```

### Explanation 

There are four files which provide pinger functionality.

```console
 ls -lh cmd/pinger 
total 16K
-rw-rw-r-- 1 dev dev 1.1K Nov  9 16:34 config.go
-rw-rw-r-- 1 dev dev 3.7K Nov  9 16:34 main.go
-rw-rw-r-- 1 dev dev  452 Nov  9 16:34 main_test.go
-rw-rw-r-- 1 dev dev  768 Nov  9 16:34 routes.go
```

`main.go` imported all require packages i.e. Package fmt implements formatted I/O with functions analogous to C's printf and scanf.
Then it define vaiables like DefaultPingTimeout and many more. Using package viper, which provides complete configuration solution. Which initialise the configuration by refering config.go.

Which includes following constants.

```console
const DefaultInterface = "0.0.0.0"
const DefaultPort = 8000
const DefaultTargetHost = "localhost"
const DefaultTargetPath = "/"
const DefaultTargetPort = 8000
const DefaultTargetProto = "http"
```

Which used to build ping request to **Target Server**. It is configured to listen on "0.0.0.0" interface with default port set to 8000. Using ```http.NewRequest`` function call, it sed "GET" request to default interface and port. 
Using Package http, which provides HTTP client and server implementations. Which print HTTP GET request from 127.0.0.1:51558 to localhost:8000.

```
 server|2019/11/10 08:36:31 < localhost:8000 <- 127.0.0.1:51558 | HTTP/1.1 GET / 
```

req, err := http.NewRequest("GET", config.getTargetURL(), nil)


This server listen and server on localhost at port 8000 using http protocol. Using serviceLogger it prints response status using getTargetURL function.


``` http://localhost:8000/ -> '200 OK' ```

fmt.Sprintf("%s://%s:%v%s", c.TargetProto, c.TargetHost, c.TargetPort, c.TargetPath)

```service|2019/11/10 08:36:30 > http://localhost:8000/ -> '200 OK'```

The ```response.Status``` is return by client.Do(req) e.g. '200 OK'.

In short server sending HTTP request to itself on port 8000 and responding back with success.


### More Explanation

This program expose two more end points (with root endpoint as "/"). The code is in the routes.go.
Where createMux returns the multiplexer we use to route our requests. 
"/" handle the request and repsond back with "hello world".
"/healthz" handle the request and respond back with "alive".
"/readyz" handle the request and respond back with "ready" (if http.StatusOK) or "not ready" (if http.StatusTeapot).


```bash
 server|2019/11/10 08:36:37 < localhost:8000 <- 172.17.0.1:54924 | HTTP/1.1 GET /livez 
 server|2019/11/10 08:36:28 < localhost:8000 <- 172.17.0.1:54894 | HTTP/1.1 GET /readyz 

```

<<<<<<< HEAD
=======


## Need to troubleshot

### Semantic versioning


[Refernce](https://medium.com/wbaa/automatic-semantic-versioning-using-gitlab-merge-request-labels-f58aae4f82cc)

Used project called [gitlab-semantic-versioning](https://github.com/mrooding/gitlab-semantic-versioning), but it failed to extract merge request from commit message. Need to troubleshoot this issue in dept.


```console

$ python3 /version-update/version-update.py
Traceback (most recent call last):
  File "/version-update/version-update.py", line 89, in <module>
    sys.exit(main())
  File "/version-update/version-update.py", line 80, in main
    version = bump(latest)
  File "/version-update/version-update.py", line 45, in bump
    labels = retrieve_labels_from_merge_request(merge_request_id)
  File "/version-update/version-update.py", line 36, in retrieve_labels_from_merge_request
    gl.auth()
  File "/usr/local/lib/python3.7/site-packages/gitlab/__init__.py", line 201, in auth
    self._token_auth()
  File "/usr/local/lib/python3.7/site-packages/gitlab/__init__.py", line 214, in _token_auth
    self.user = self._objects.CurrentUserManager(self).get()
  File "/usr/local/lib/python3.7/site-packages/gitlab/exceptions.py", line 246, in wrapped_f
    return f(*args, **kwargs)
  File "/usr/local/lib/python3.7/site-packages/gitlab/mixins.py", line 68, in get
    server_data = self.gitlab.http_get(self.path, **kwargs)
  File "/usr/local/lib/python3.7/site-packages/gitlab/__init__.py", line 528, in http_get
    streamed=streamed, **kwargs)
  File "/usr/local/lib/python3.7/site-packages/gitlab/__init__.py", line 502, in http_request
    response_body=result.content)
gitlab.exceptions.GitlabAuthenticationError: 401: 401 Unauthorized
ERROR: Job failed: exit code 1

```

After adding new group and adding NPA_USERNAME and NPA_PASSWORD [  the Group variable settings in Settings -> CI / CD -> Variables ].

Got one more error.

```
$ python3 /version-update/version-update.py
Traceback (most recent call last):
  File "/version-update/version-update.py", line 89, in <module>
    sys.exit(main())
  File "/version-update/version-update.py", line 80, in main
    version = bump(latest)
  File "/version-update/version-update.py", line 44, in bump
    merge_request_id = extract_merge_request_id_from_commit()
  File "/version-update/version-update.py", line 27, in extract_merge_request_id_from_commit
    raise Exception(f"Unable to extract merge request from commit message: {message}")
Exception: Unable to extract merge request from commit message: b'Updated documentation\n\n'
ERROR: Job failed: exit code 1

```

Testing using new branch as merge.

Finally it works!!!

Need to do two things:

```
$ echo $IMAGE and $IMAGE_NAME and $TAG
registry.gitlab.com/amitkarpe/devops2:3.0.1-2-g86fb5bf and registry.gitlab.com/amitkarpe/devops2 and 3.0.1-2-g86fb5bf
$ docker login registry.gitlab.com -u amitkarpe -p 8zP5Cxsx6RzyxnjVb851
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
$ echo login YES
login YES
$ echo New Code
New Code
$ docker pull $IMAGE_NAME
Using default tag: latest
latest: Pulling from amitkarpe/devops2
a6113cdc8328: Pulling fs layer
7eb31b36cfbd: Pulling fs layer
a6113cdc8328: Verifying Checksum
a6113cdc8328: Download complete
a6113cdc8328: Pull complete
7eb31b36cfbd: Verifying Checksum
7eb31b36cfbd: Download complete
7eb31b36cfbd: Pull complete
Digest: sha256:1060501b2a50c92cad75c1de38e6cde2aed094eae86dc9f725dfd46d7c458cc1
Status: Downloaded newer image for registry.gitlab.com/amitkarpe/devops2:latest
$ docker tag $IMAGE_NAME $IMAGE
$ docker push $IMAGE
The push refers to repository [registry.gitlab.com/amitkarpe/devops2]
b80c40fdcb8e: Preparing
d74aa7c8ea79: Preparing
d74aa7c8ea79: Layer already exists
b80c40fdcb8e: Layer already exists
3.0.1-2-g86fb5bf: digest: sha256:1060501b2a50c92cad75c1de38e6cde2aed094eae86dc9f725dfd46d7c458cc1 size: 735
Job succeeded
```
>>>>>>> fe3c5d6e7cd64b9856d35b127d70830eb75b48dc
